"""This script takes an input fasta file of candidate peptides
and a fasta file of proven peptides, performs a pairwise sequence 
alignment and outputs the top 10 matches based on the alignment score"""

import argparse 
from Bio import SeqIO
from Bio import pairwise2
import pandas as pd


def shortlist_candidates(candidates, target, results, top):
    
    shortlisted = [] # a list of shortlisted peptides 
    
    for seq in SeqIO.parse(target, 'fasta'): # parses known peptides 

        all_results = [] # list of all candidate alignments 

        for candidate in SeqIO.parse(candidates, 'fasta'): 
            alignment = {} # dictionary for each alignment
            alignments = pairwise2.align.localxx(candidate, seq, score_only=True) # perform a pairwise alignment, returning only the score
            alignment['ID'] = candidate.id.strip('identifier:') # Retrieve the peptide identifier, remove identifier string
            alignment['Sequence'] = candidate.seq # The sequence 
            alignment['Score'] = alignments # retrieve score of the alignment
            alignment['Proven peptide'] = seq.id.strip('identifier:')
            all_results.append(alignment) # append dictonary to list 
        
        for result in all_results:
            shortlisted.append(result)
    
    shortlisted_ranked = sorted(shortlisted, key=lambda d: d['Score'], reverse=True) # Sort by alignment score in reverse order
    shortlisted_top10 = shortlisted_ranked[:top] # Select top peptides based on default of 10, or value supplied

    df = pd.DataFrame(shortlisted_top10) # create a dataframe 
    df.to_csv(results) # convert dataframe to a csv file, with the name specified in outfile arg


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--candidate", type=str, required=True, help='Path to input fasta file with peptide candidates')
    parser.add_argument("-t", "--targetseq", type=str, required=True, help='Path to input fasta file with known peptide sequences')
    parser.add_argument("-o", "--outfile", type=str, required=True, help='Path to output csv file with results i.e., /home/candidate_shortlist.csv')
    parser.add_argument("-s", "--shortlist", type=int, required=False, default=10, help='Number of peptide targets to shortlist, default is 10')
    
    args = parser.parse_args()

    candidates = args.candidate
    target = args.targetseq
    results = args.outfile

    if args.shortlist:
        top = args.shortlist
    else:
        top = 10
    shortlist_candidates(candidates, target, results, top)


