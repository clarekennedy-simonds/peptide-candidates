# peptide-candidates

#### 15/04/2022 ####
Author: Clare Kennedy-Simonds

## Description
This project shortlists candidate peptides to target a protein, based on their similarity to validated agonists/antagonists.
Local pairwise alignments are performed and ranked by score, returning the top 10 targets.

## Installation
Can be run within a docker instance;
```
docker pull biopython/biopython-sql
docker run -it -v /path/to/working/directory/:/input biopython/biopython-sql /bin/bash
python3 ../input/peptide-shortlist.py -h
```

** Note that docker entrypoint is the biopython directory - mounts will be one level up

If docker is not being used, programme requirements are outlined in requirements.txt

## Requirements

1. A fasta file with candidate peptides 
2. A fasta file with proven peptides

## Usage
```
python3 ../input/peptide-shortlist.py -c ../input/candidate_peptides.fasta -t ../input/proven_peptides.fasta -o ../input/results.csv
```
